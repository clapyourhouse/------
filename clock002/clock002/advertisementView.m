//
//  advertisementView.m
//  clock002
//
//  Created by KitamuraShogo on 13/02/26.
//  Copyright (c) 2013年 KitamuraShogo. All rights reserved.
//

#import "advertisementView.h"
#import "motionView.h"
#import "SVProgressHUD.h"
#import "ViewController.h"

@interface advertisementView ()

@end

@implementation advertisementView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
       // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UIWebView *wv1 = [[UIWebView alloc] init];
    wv1.delegate = self;
    wv1.frame = CGRectMake(0, 0, 160, 300);
    wv1.scalesPageToFit = YES;
    [self.view addSubview:wv1];
    
    NSURL *url1 = [NSURL URLWithString:@"http://www.yahoo.co.jp"];
    NSURLRequest *req1 = [NSURLRequest requestWithURL:url1];
    [wv1 loadRequest:req1];
    
    
    UIWebView *wv2 = [[UIWebView alloc] init];
    wv2.delegate = self;
    wv2.frame = CGRectMake(160, 0, 160, 300);
    wv2.scalesPageToFit = YES;
    [self.view addSubview:wv2];
    
    NSURL *url2 = [NSURL URLWithString:@"http://www.google.jp"];
    NSURLRequest *req2 = [NSURLRequest requestWithURL:url2];
    [wv2 loadRequest:req2];

}

// ページ読込開始直後に呼ばれるデリゲートメソッド
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    // 2'.表示するメッセージに「ロード中です」を指定して、アラートビューを表示したときのようなオーバーレイを表示
    [SVProgressHUD showWithStatus:@"ロード中です" maskType:SVProgressHUDMaskTypeGradient];

}

// ページ読込終了直後に呼ばれるデリゲートメソッド
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    // 3.SVProgressHUDを非表示にする
    [SVProgressHUD dismiss];
    // 3'.読み込みに成功した旨を表示し、SVProgressHUDを非表示にする
    [SVProgressHUD showSuccessWithStatus:@"ロード完了！"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    [SVProgressHUD release];
    [super dealloc];
}

@end
